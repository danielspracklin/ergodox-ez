# ergodox-ez
My ErgoDox EZ layout

This is the layout I've chosen for my ErgoDox EZ Glow.

Major changes so far:

- I switched to Colemak in May 2020 and have mapped L1 to Colemak accordingly, but have kept L0 on QWERTY to take advantage of muscle memory when typing passwords
- I added an extend layer inspired by [DreymaR's layout](https://forum.colemak.com/topic/2014-extend-extra-extreme/) but with reordered shift, control and alt
- I moved escape to the typical caps lock layout
- Thumb clusters handle (from left to right) space, backspace, Extend and enter; holding space and enter give control and alt
- I enabled auto-shift and have reduced the trigger length
- I've really come to appreciate space-cadet shifts, which now have me desperately mashing shift when on a non-QMK board

I've written [a blog post](https://danielspracklin.gitlab.io/jekyll/update/2020/07/09/ergodox-ez-the-first-two-months.html) about the rationale for this layout.
